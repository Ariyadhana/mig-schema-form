import React from "react";
import { getEmptyObject } from "../../inc/schema";
import { ErrorObject } from "ajv";
import { SchemaFormGroup } from "./SchemaFormGroup";
import { trim } from "lodash";
import { t } from "../../inc/stxt";
import { oas31 } from "openapi3-ts";
import { IPropConfig } from "../../type/field";

type ISchemaFormBodyProps<T> = {
  children?: React.ReactNode;
  config?: { [propName: string]: IPropConfig };
  errors?: ErrorObject[];
  form?: string;
  instancePath?: string;
  onChange: (value: T) => void;
  schema: oas31.SchemaObject;
  value?: T;
};

const formatError = (error: ErrorObject) => {
  // eg. :"/netfeedrQueries/0"
  const pathNibbles = trim(error.instancePath, "/").split("/").map(t);
  if (!error.instancePath && !error.message) {
    return t("pleaseCheckTheForm");
  }

  return `${error.instancePath ? `${pathNibbles.join(" > ")}: ` : ""}${t(
    error.message || "pleaseCheckThisValue"
  )}`;
};

function SchemaFormBody<T>({
  children,
  config,
  form,
  instancePath = "",
  schema,
  onChange,
  errors,
  value,
}: ISchemaFormBodyProps<T>) {
  const { properties = {}, required = [] } = schema;
  const schemaFormValue: T = value ? value : getEmptyObject(schema);

  const setPropValue = React.useCallback(
    (propName: string, propValue: any) => {
      onChange({
        ...schemaFormValue,
        [propName]: propValue,
      });
    },
    [onChange, schemaFormValue]
  );

  return (
    <div className="schema-form-body">
      {errors?.length ? (
        <div className="alert alert--danger">
          <ul>
            {errors.map((error, errorIndex) => (
              <li key={errorIndex}>{formatError(error)}</li>
            ))}
          </ul>
        </div>
      ) : null}
      {Object.keys(properties).map((propName) => {
        const propSchema = properties[propName] as oas31.SchemaObject;
        const propInstancePath = `${instancePath}/${propName}`;
        const propConfig = config ? config[propName] : undefined;
        const FormGroup = propConfig?.FormGroup || SchemaFormGroup;
        const propErrors = errors
          ? errors.filter((error) =>
              error.instancePath?.startsWith(propInstancePath)
            )
          : [];
        if (
          (propSchema.readOnly || propConfig?.hidden) &&
          propConfig?.hidden !== false
        ) {
          return null;
        }
        const isRequired = required.includes(propName);
        return (
          <FormGroup
            key={propName}
            form={form}
            isRequired={isRequired}
            propConfig={propConfig}
            propErrors={propErrors}
            propInstancePath={propInstancePath}
            propName={propName}
            propSchema={propSchema}
            propValue={schemaFormValue[propName as keyof T]}
            setPropValue={setPropValue}
            {...propConfig?.formGroupProps}
          />
        );
      })}
      {children}
    </div>
  );
}

export default React.memo(SchemaFormBody) as typeof SchemaFormBody;
