import { StylesConfig } from "react-select/dist/declarations/src/styles";
import { REACT_SELECT_MENU_LIST_WIDTH } from "./MenuList";

// Based on https://github.com/JedWatson/react-select/issues/1322#issuecomment-1081963162
const reactSelectStyles: StylesConfig<any> = {
  container: (base: any) => ({
    ...base,
    minWidth: 225,
  }),
  menu: (base: any) => ({
    ...base,
    width: REACT_SELECT_MENU_LIST_WIDTH,
    zIndex: 2,
  }),
  input: (provided, state) => ({
    ...provided,
    width: 225,
    height: 20,
    display: "flex",
    alignItems: "center",
  }),
  singleValue: (provided, state) => ({
    ...provided,
    marginTop: 2,
  }),
};

export default reactSelectStyles;
