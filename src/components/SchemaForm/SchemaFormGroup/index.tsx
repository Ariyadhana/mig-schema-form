import React from "react";
import SchemaFormControl from "./SchemaFormControl";
import { ISchemaFormGroupProps } from "../../../type/field";
import { t } from "../../../inc/stxt";

export const SchemaFormGroup = React.memo(
  ({
    form,
    isRequired,
    propConfig,
    propErrors,
    propName,
    propSchema,
    propValue,
    setPropValue,
  }: ISchemaFormGroupProps) => {
    const onPropChange = React.useCallback(
      (newValue: any) => {
        setPropValue(propName, newValue);
      },
      [propName, setPropValue]
    );

    const schemaFormControl = React.useMemo(() => {
      const FormControl = propConfig?.FormControl || SchemaFormControl;
      return (
        <FormControl
          form={form}
          isDisabled={!!propConfig?.isDisabled}
          isRequired={isRequired}
          onPropChange={onPropChange}
          propConfig={propConfig}
          // propErrors={propErrors}
          // propInstancePath={propInstancePath}
          propName={propName}
          propSchema={propSchema}
          propValue={propValue}
          {...propConfig?.formControlProps}
        />
      );
    }, [
      form,
      isRequired,
      onPropChange,
      propConfig,
      propName,
      propSchema,
      propValue,
    ]);

    const className = `schema-form-group schema-form-group--type-${
      propSchema.type
    } schema-form-group--name-${propName}${
      propErrors.length ? " schema-form-group--error" : ""
    }`;

    switch (propSchema.type) {
      case "boolean":
        return (
          <div className={className} key={`${propName}`}>
            <label className="label" htmlFor={`${propName}`}>
              {schemaFormControl}
              {propConfig?.label === undefined ? t(propName) : propConfig.label}
              {/*
              // https://www.notion.so/02f42b8cf8444eb2812940a06586174b?v=5e83c3815f714cebafbdade5777e84d8&p=05f82f5fcaad43a482f4dfb4ec5bfe2e&pm=s
              // Als accountmanager wil ik dat de * bij "Breng de sales-afdeling op de hoogte bij een wijziging" weg gaat, zodat ik niet verward ben dat het verplicht is of niet
              {isRequired ? " *" : ""}*/}
            </label>
          </div>
        );

      case "object":
        return (
          <fieldset key={propName} className={className}>
            <legend>{t(propName)}</legend>
            {schemaFormControl}
          </fieldset>
        );

      default:
        const allowedChars =
          propSchema.maxLength && propValue !== undefined
            ? propSchema.maxLength - propValue.length
            : 0;
        return (
          <div className={className} key={`${propName}`}>
            <div className="schema-form-group__labeled-control">
              <label className="label" htmlFor={`${propName}`}>
                {propConfig?.label === undefined
                  ? t(propName)
                  : propConfig.label}
                {isRequired ? " *" : ""}
              </label>
              <div className="flex-1">
                {schemaFormControl}
                {allowedChars > 0 ? (
                  <div className="schema-form-group__helper-text">
                    {allowedChars === 1
                      ? "1 more character allowed"
                      : `${allowedChars} more characters allowed`}
                  </div>
                ) : null}
                {allowedChars < 0 ? (
                  <div
                    className="schema-form-group__helper-text"
                    style={{ color: "red" }}
                  >
                    {allowedChars === -1
                      ? "1 character too much"
                      : `${-allowedChars} characters too much`}
                  </div>
                ) : null}
              </div>
            </div>
            {propErrors.length ? (
              <div className="schema-form-group__error">
                {t(propErrors[0].message || "pleaseCheckThisValue")}
              </div>
            ) : null}
          </div>
        );
    }
  }
);
