import SchemaFormBody from "../../index";
import React from "react";
import DatePicker from "react-datepicker";
import Select from "react-select";
import { t } from "../../../../inc/stxt";
import { localeFormat } from "../../../../inc/date";
import { oas31 } from "openapi3-ts";
import reactSelectStyles from "../../react-select/styles";
import MenuList from "../../react-select/MenuList";
import { ISchemaFormControlProps } from "../../../../type/field";

const SchemaFormControl = ({
  form,
  isDisabled,
  isRequired,
  onPropChange,
  propConfig,
  // propErrors,
  // propInstancePath,
  propName,
  propSchema,
  propValue,
}: ISchemaFormControlProps) => {
  let formControlProps: React.InputHTMLAttributes<
    HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement
  > = {
    ...propConfig?.formControlProps,
    disabled: isDisabled,
    id: propName,
  };

  switch (propSchema.type) {
    case "boolean":
      return (
        <input
          type="checkbox"
          form={form}
          value={1}
          checked={propValue || false}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            onPropChange(e.target.checked);
          }}
          {...formControlProps}
        />
      );

    case "array":
      const itemsSchema = propSchema.items as oas31.SchemaObject;
      switch (itemsSchema.type) {
        case "string":
          if (itemsSchema.enum) {
            return (
              <Select
                form={form}
                menuPlacement="auto"
                components={{ MenuList }}
                styles={reactSelectStyles}
                options={itemsSchema.enum.map((option) => ({
                  value: option,
                  label: t(option),
                }))}
                isDisabled={isDisabled}
                required={isRequired}
                isMulti
                value={(propValue || []).map((option: string) => ({
                  value: option,
                  label: t(option),
                }))}
                onChange={(newValue) => {
                  onPropChange(newValue.map((option: any) => option.value));
                }}
              />
            );
          }
      }
      console.log(propName, itemsSchema);
      throw new Error("Unsupported itemsSchema");

    case "integer":
      return (
        <input
          form={form}
          type="number"
          required={isRequired}
          value={propValue || ""}
          onChange={(
            e: React.ChangeEvent<
              HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement
            >
          ) => {
            const parsedValue = parseInt(e.target.value);
            onPropChange(
              propConfig?.extractValue
                ? propConfig.extractValue(e)
                : isNaN(parsedValue)
                ? undefined
                : parsedValue
            );
          }}
          {...formControlProps}
        />
      );

    case "string":
      const onStringInputChange = (
        e: React.ChangeEvent<
          HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement
        >
      ) => {
        onPropChange(
          propConfig?.extractValue ? propConfig.extractValue(e) : e.target.value
        );
      };
      const type = propSchema.format || "text";

      if (propSchema.format === "date") {
        return (
          <DatePicker
            form={form}
            dateFormat="dd-MM-yyyy"
            selected={propValue ? new Date(propValue) : null}
            onChange={(date: Date) =>
              onPropChange(date ? localeFormat(date, "yyyy-MM-dd") : undefined)
            }
            required={isRequired}
            disabled={isDisabled}
            {...(formControlProps as any)}
          />
        );
      }

      if (propSchema.enum) {
        return (
          <div style={{ width: 250 }}>
            <Select
              form={form}
              menuPlacement="auto"
              options={propSchema.enum.map((option) => ({
                value: option,
                label: t(option),
              }))}
              isDisabled={isDisabled || propSchema.enum.length < 2}
              required={isRequired}
              value={
                propValue
                  ? {
                      value: propValue,
                      label: t(propValue),
                    }
                  : null
              }
              onChange={(option) => onPropChange(option!.value)}
            />
          </div>
        );
      }

      return propSchema.maxLength &&
        propSchema.maxLength > 512 &&
        !propSchema.format ? (
        <textarea
          form={form}
          required={isRequired}
          onChange={onStringInputChange}
          value={propValue || ""}
          {...{
            ...formControlProps,
            style: undefined,
          }}
        />
      ) : (
        <input
          type={type === "uri" ? "url" : type}
          form={form}
          required={isRequired}
          value={propValue || ""}
          onChange={onStringInputChange}
          {...formControlProps}
        />
      );

    case "object":
      return (
        <SchemaFormBody<any>
          form={form}
          schema={propSchema}
          value={propValue}
          onChange={onPropChange}
          {...formControlProps}
        />
      );

    default:
      console.log(propSchema);
      throw new Error("Unsupported schema");
  }
};

export default React.memo(SchemaFormControl);
