import React from "react";
import { oas31 } from "openapi3-ts";
import { ErrorObject } from "ajv";

export interface ISchemaFormGroupProps<T = any> {
  form?: string;
  isRequired: boolean;
  propConfig?: IPropConfig;
  propErrors: ErrorObject[];
  propInstancePath: string;
  propName: string;
  propSchema: oas31.SchemaObject;
  propValue: T;
  setPropValue: (propName: string, value: T) => void;
}

export interface ISchemaFormControlProps<T = any> {
  form?: string;
  isDisabled: boolean;
  isRequired: boolean;
  onPropChange: (newValue: T) => void;
  propConfig?: IPropConfig;
  // propErrors: ErrorObject[];
  // propInstancePath: string;
  propName: string;
  propSchema: oas31.SchemaObject;
  propValue: T;
}
export interface IPropConfig {
  FormControl?: (
    props: ISchemaFormControlProps & { [formControlProp: string]: any }
  ) => React.ReactElement | null;
  FormGroup?: (
    props: ISchemaFormGroupProps & { [formGroupProp: string]: any }
  ) => React.ReactElement | null;
  extractValue?: (e: React.ChangeEvent<any>) => any;
  formControlProps?: { [formControlProp: string]: any };
  formGroupProps?: { [formGroupProp: string]: any };
  hidden?: boolean;
  isDisabled?: boolean;
  label?: React.ReactNode;
}

export type TInputType =
  | "string"
  | "integer"
  | "date"
  | "date-time"
  | "boolean"
  | "array"
  | "object";
export interface IFieldConfig {
  extractValue?: (
    key: string,
    val: string,
    index?: number,
    type?: TInputType
  ) => any;
  schema?: oas31.SchemaObject;
  disabled?: boolean;
  hidden?: boolean;
  component?: string;
  style?: React.CSSProperties;
  inputType?: "select" | "input" | "date";
  options?: { [key: string]: string }[];
  format?: "url";
  dateFormat?: string;
  title?: string;
  virtualizedSelect?: boolean;
}
