import { uncamel } from "./string";

const db: { [key: string]: string } = {};

export function t(key: string) {
  return db[key] || uncamel(key);
}
