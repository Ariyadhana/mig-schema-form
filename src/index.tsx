import SchemaForm from "./components/SchemaForm";
import {
  IFieldConfig,
  TInputType,
  IPropConfig,
  ISchemaFormControlProps,
  ISchemaFormGroupProps,
} from "./type/field";
export type {
  IFieldConfig,
  TInputType,
  IPropConfig,
  ISchemaFormControlProps,
  ISchemaFormGroupProps,
};
export { SchemaForm };
