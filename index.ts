import SchemaForm from "./src/components/SchemaForm";
import {
  IFieldConfig,
  TInputType,
  IPropConfig,
  ISchemaFormControlProps,
  ISchemaFormGroupProps,
} from "./src";
export type {
  IFieldConfig,
  TInputType,
  IPropConfig,
  ISchemaFormControlProps,
  ISchemaFormGroupProps,
};
export { SchemaForm };
